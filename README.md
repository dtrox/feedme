# feedme

Python asyncio publisher/subscriber events. No dependencies.

## Example

```python
import asyncio
import uuid

from feedme import Event, Feed


feed = Feed("example")


class SimpleEvent(Event[uuid.UUID]):
    pass


@feed.publisher()
async def example_emitter():
    return [SimpleEvent(uuid.uuid4())]


@feed.subscriber()
async def foo_handler(event: SimpleEvent):
    print(f"foo: {event.value}", flush=True)


@feed.subscriber()
async def bar_handler(event: SimpleEvent):
    print(f"bar: {event.value}", flush=True)


async def main():
    try:
        await asyncio.wait_for(feed.run(), 10)
    except asyncio.TimeoutError:
        pass


if __name__ == "__main__":
    asyncio.run(main())

```
