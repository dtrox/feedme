import asyncio

import pytest

from feedme import Event, Feed
from feedme.event import DBDIR

_events_log = []


class MyEvent(Event):
    pass


event = MyEvent("event")


async def emitter():
    return [event]


async def handler(event: MyEvent):
    _events_log.append(event)


@pytest.mark.asyncio
async def test_run():
    appname = "__testapp"
    try:
        feed = Feed(appname)

        feed.add_publisher(emitter)
        feed.add_subscriber(handler)

        try:
            await asyncio.wait_for(feed.run(), 0.1)
        except asyncio.TimeoutError:
            pass

        assert len(_events_log) == 1
        assert _events_log[0] is event

    finally:
        (DBDIR / f"{appname}.db").unlink(missing_ok=True)
