import asyncio
import datetime as dt
import uuid

from feedme import Event, Feed

feed = Feed("example")


class SimpleEvent(Event):
    pass


class CustomEvent(Event[dt.datetime]):
    def key(self):
        return self.value.replace(
            second=self.value.second - self.value.second % 5, microsecond=0
        )


@feed.publisher(delay=1)
async def example_emitter() -> list[Event]:
    return [SimpleEvent(uuid.uuid4()), CustomEvent(dt.datetime.now(dt.timezone.utc))]


@feed.subscriber()
async def foo_handler(event: SimpleEvent):
    print(f"foo: {event}", flush=True)


@feed.subscriber()
async def bar_handler(event: SimpleEvent):
    print(f"bar: {event}", flush=True)


@feed.subscriber()
async def baz_handler(event: CustomEvent):
    print(f"baz: {event}", flush=True)


async def main():
    try:
        await asyncio.wait_for(feed.run(), 10)
    except asyncio.TimeoutError:
        pass


if __name__ == "__main__":
    # import logging
    # logging.basicConfig(level=logging.INFO, force=True)
    asyncio.run(main())
