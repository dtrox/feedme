"""An example showing how to emit events from a handler."""

import asyncio
import datetime

from feedme import Event, Feed

feed = Feed("example_subpub")


# optional, type hint the value for handlers
class ExampleEvent(Event[datetime.datetime]):
    pass


@feed.publisher(delay=1)
async def example_emitter():
    return [ExampleEvent(datetime.datetime.now(tz=datetime.timezone.utc))]


class AnotherEvent(Event[datetime.time]):
    pass


@feed.subscriber()
async def foo_handler(event: ExampleEvent):
    print(f"foo: {event.value}", flush=True)
    return [
        AnotherEvent((event.value + datetime.timedelta(hours=i)).time())
        for i in range(3)
    ]


@feed.subscriber()
async def bar_handler(event: AnotherEvent):
    print(f"bar: {event.value}", flush=True)


async def main():
    try:
        await asyncio.wait_for(feed.run(), 10)
    except asyncio.TimeoutError:
        pass


if __name__ == "__main__":
    # import logging
    # logging.basicConfig(level=logging.INFO)
    asyncio.run(main())
