import asyncio
import datetime
import sqlite3
from concurrent.futures import ThreadPoolExecutor
from contextlib import contextmanager
from pathlib import Path
from typing import Generic, TypeVar, Union

X = TypeVar("X")


class Event(Generic[X]):
    """Event for a feed.

    Sub-class this class to define unique events.

    Override `key()` to change how the event is keyed in the database.
    """

    def __init__(self, value: X):
        self.value = value

    def key(self):
        return str(self.value)

    def __str__(self):
        return f"{self.__class__.__name__}__{self.key()}"

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({repr(self.value)})"


DBDIR = Path.home() / ".cache" / "feedme"
DBDIR.mkdir(mode=0o755, parents=True, exist_ok=True)


_INIT = """
create table if not exists feed_event(
    id integer primary key,
    event_key text not null,
    created_at timestamp not null
);
"""


class EventDB:
    """Database client for events."""

    def __init__(self, name: str, *, dbdir: Union[str, Path] = DBDIR):
        self.name = name
        self.filepath = Path(dbdir) / f"{self.name}.db"
        self._lock = None
        self._pool = ThreadPoolExecutor()
        with self._con() as con:
            con.executescript(_INIT)

    @contextmanager
    def _con(self):
        con = sqlite3.connect(self.filepath)
        try:
            with con:
                yield con
        finally:
            con.close()

    async def _to_thread(self, coro, *args, **kwargs):
        return await asyncio.get_event_loop().run_in_executor(
            self._pool, coro, *args, **kwargs
        )

    def _exists(self, event: Event) -> bool:
        query = "select exists(select 1 from feed_event where event_key = ? limit 1);"
        params = (str(event),)
        with self._con() as con:
            result = next(con.execute(query, params))[0]
        return bool(result)

    async def exists(self, event: Event) -> bool:
        return await self._to_thread(self._exists, event)

    def _insert(self, event: Event):
        with self._con() as con:
            con.execute(
                "insert into feed_event ( event_key, created_at ) values ( ?, ? );",
                (
                    str(event),
                    datetime.datetime.now(tz=datetime.timezone.utc).isoformat(),
                ),
            )

    async def insert(self, event: Event):
        if self._lock is None:
            self._lock = asyncio.Lock()

        async with self._lock:
            result = await self._to_thread(self._insert, event)

        return result
