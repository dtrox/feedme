import asyncio
import inspect
import logging
import typing
from collections.abc import (
    AsyncGenerator,
    Awaitable,
    Callable,
    Iterable,
)
from typing import Any, Optional, Union

from feedme.event import Event, EventDB

logger = logging.getLogger(__name__)


DEFAULT_PUBLISH_DELAY = 0
"""default num seconds between emitter runs"""


class _Publisher:
    def __init__(self, db: EventDB, name: str):
        self.db = db
        self.name = name

    async def publish(self, event: Event, queue: asyncio.Queue):
        """Store the event in the database and emit it to the queue.

        If the event is already in the database, does nothing.
        """
        if await self.db.exists(event):
            # event already published, do nothing
            return False
        await self.db.insert(event)
        await queue.put(event)
        logger.info(
            "%s %s emitted event %s",
            self.__class__.__name__,
            self.name,
            str(event),
        )
        return True

    async def run(self):
        raise NotImplementedError()


Emitter = Callable[[], Union[Awaitable[Iterable[Event]], AsyncGenerator[Event, None]]]
"""An async function that emits events."""


class Publisher(_Publisher):
    """Publishes events to a `Feed`."""

    emitter: Emitter

    def __init__(
        self, db: EventDB, emitter: Emitter, *, delay: float = DEFAULT_PUBLISH_DELAY
    ):
        super().__init__(db, emitter.__name__)
        self.emitter = emitter
        self.delay = delay

    async def run(self, queue: asyncio.Queue):
        """Run the publisher indefinitely.

        Publishes events from the attached emitter to the `queue`.
        """
        if inspect.isasyncgenfunction(self.emitter):
            async for event in self.emitter():
                await self.publish(event, queue)
                await asyncio.sleep(self.delay)

        else:
            while True:
                coro = self.emitter()
                assert isinstance(coro, Awaitable)

                try:
                    for event in await coro:
                        await self.publish(event, queue)
                except Exception as err:
                    logger.exception(f"publisher {self.name} error: {err}")

                await asyncio.sleep(self.delay)


AnyEvent = typing.TypeVar("AnyEvent", bound=Event)


Handler = Callable[
    [AnyEvent],
    Union[
        Awaitable[Optional[Union[Event, Iterable[Event]]]], AsyncGenerator[Event, None]
    ],
]
"""An async function that handles an event as the only argument.

Can return events to be published back to the queue.
"""


def _get_event_type_from_annotation(handler: Handler):
    try:
        return list(typing.get_type_hints(handler).values())[0]
    except IndexError:
        raise ValueError("no type hint on handler")


class Subscriber(_Publisher):
    """Handles events from a `Feed`."""

    handler: Handler

    def __init__(
        self, db: EventDB, handler: Handler, event_type: Optional[type[Event]] = None
    ):
        super().__init__(db, handler.__name__)
        self.handler = handler
        self.event_type = event_type or _get_event_type_from_annotation(self.handler)

    async def handle(self, event: Event, pub_queue: asyncio.Queue):
        if event is None or not isinstance(event, self.event_type):
            # event not relevant to this subscriber
            return

        initiator = event
        publish_tasks: list[asyncio.Task[bool]] = []

        try:
            if inspect.isasyncgenfunction(self.handler):
                async for event in self.handler(initiator):
                    task = asyncio.create_task(self.publish(event, pub_queue))
                    publish_tasks.append(task)

            else:
                result: Any = await self.handler(initiator)  # type: ignore
                if result:
                    try:
                        events = iter(result)
                    except TypeError:
                        events = [result]
                    for event in events:
                        task = asyncio.create_task(self.publish(event, pub_queue))
                        publish_tasks.append(task)

        except Exception as err:
            logger.exception(
                f"subscriber `{self.name}` error while handling event `{event}`: {err}"
            )

        if publish_tasks:
            await asyncio.gather(*publish_tasks)

        logger.info(
            "%s %s handled event %s",
            self.__class__.__name__,
            self.name,
            str(initiator),
        )


class Feed:
    """Pub/sub feed.

    The `name` param sets the name of the event database for this feed. Feeds with the
    same name use the same event database.
    """

    def __init__(self, name: str):
        self.name = name
        self.db = EventDB(name)
        self.publishers: list[Publisher] = []
        self.subscribers: list[Subscriber] = []

    def add_publisher(self, emitter: Emitter, *, delay: float = DEFAULT_PUBLISH_DELAY):
        """Add a publisher to this feed."""
        self.publishers.append(Publisher(self.db, emitter, delay=delay))

    def publisher(self, *, delay: float = DEFAULT_PUBLISH_DELAY):
        """Decorator to add a publisher to this feed."""

        def wrapper(emitter: Emitter):
            self.add_publisher(emitter, delay=delay)
            return emitter

        return wrapper

    def add_subscriber(
        self, handler: Handler, event_type: Optional[type[Event]] = None
    ):
        """Add a subscriber to this feed."""
        self.subscribers.append(Subscriber(self.db, handler, event_type=event_type))

    def subscriber(self, event_type: Optional[type[Event]] = None):
        """Decorator to add a subscriber to this feed."""

        def wrapper(handler: Handler):
            self.add_subscriber(handler, event_type=event_type)
            return handler

        return wrapper

    def include(self, other: "Feed"):
        """Add all publishers and subscribers from another feed to this feed."""
        self.publishers.extend(other.publishers)
        self.subscribers.extend(other.subscribers)

    async def _run(self, queue: asyncio.Queue):
        """Distribute events to subscribers."""
        tasks: set[asyncio.Task] = set()
        while True:
            event = await queue.get()

            if event is not None:
                for subscriber in self.subscribers:
                    task = asyncio.create_task(subscriber.handle(event, queue))
                    tasks.add(task)

            queue.task_done()
            for task in list(tasks):
                if task.done():
                    task.result()
                    tasks.remove(task)

    async def run(self):
        """Run the publishers and subscribers in this feed indefinitely."""
        publish_queue = asyncio.Queue()

        publishers = [p.run(publish_queue) for p in self.publishers]

        feed = self._run(publish_queue)

        tasks = [asyncio.create_task(c) for c in (*publishers, feed)]
        done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_EXCEPTION)
        for task in pending:
            task.cancel()
        await asyncio.gather(*pending, return_exceptions=True)
        for task in done:
            task.result()

    async def __await__(self):
        await self.run()
